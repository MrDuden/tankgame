﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class EnemySpawnPoint : MonoBehaviour
    {
        public Vector3 SpawnPoint { get { return _spawnPosition; } }

        private Vector3 _spawnPosition;

        private void Awake()
        {
            _spawnPosition = transform.position;
        }
    }
}