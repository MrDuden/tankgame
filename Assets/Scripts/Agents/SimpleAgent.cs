﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace TankGame
{
    public class SimpleAgent : Agent
    {
        [SerializeField]
        private NavMeshAgent _navMeshAgent;
  
        public override bool IsMoving { get { return _navMeshAgent.velocity.magnitude > 0.1f; } }

        public override bool IsStopped { get { return _navMeshAgent.isStopped; } }

        public override void MoveTo(Vector3 position)
        {
            if (_navMeshAgent.isStopped == true)
            {
                _navMeshAgent.isStopped = false;
            }

            _navMeshAgent.destination = position;
        }

        public override void RotateToTarget(Transform target)
        {
            var direction = (new Vector3(target.position.x, 0, target.position.z) - new Vector3(transform.position.x, 0, transform.position.z)).normalized;
            var lookRotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * _navMeshAgent.angularSpeed);
        }

        public override void Stop()
        {
            _navMeshAgent.isStopped = true;
        }
    }
}
