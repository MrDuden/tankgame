﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public abstract class Agent : MonoBehaviour
    {
        public abstract bool IsMoving { get; }
        public abstract bool IsStopped { get; }

        public abstract void MoveTo(Vector3 position);
        public abstract void Stop();
        public abstract void RotateToTarget(Transform target);
    }
}
