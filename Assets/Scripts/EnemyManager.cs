﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TankGame
{
    public class EnemyManager : MonoBehaviour
    {
        private Dictionary<MobType, EnemyData> _enemyData = new Dictionary<MobType, EnemyData>();

        private void Start()
        {
            Mob.OnKilled += OnMobKilled;
            Mob.OnSpawned += OnMobSpawned;
        }

        public IMobData GetMobData(MobType enemyType)
        {
            if (_enemyData.ContainsKey(enemyType))
            {
                return _enemyData[enemyType];
            }

            var enemyData = new EnemyData(enemyType, 0);
            _enemyData.Add(enemyType, enemyData);

            return enemyData;
        }

        private void OnMobKilled(Mob mob)
        {
            _enemyData[mob.Type].Decrease();
        }

        private void OnMobSpawned(Mob mob)
        {
            if (_enemyData.ContainsKey(mob.Type))
            {
                _enemyData[mob.Type].Increase();
            }
            else
            {
                _enemyData.Add(mob.Type, new EnemyData(mob.Type, 1));
            }
        }

        [Serializable]
        private class EnemyData : IMobData
        {
            public int Count { get { return _count; } }
            public MobType Type { get { return _type; } }
            private readonly MobType _type;
            private int _count = 0;

            public EnemyData(MobType type, int count)
            {
                _type = type;
                _count = count;
            }

            public void Increase()
            {
                _count++;
            }

            public void Decrease()
            {
                _count--;
            }
        }
    }

    public interface IMobData
    {
        int Count { get; }
        MobType Type { get; }
    }
}