﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace TankGame
{
    public class PlayerWeaponPresenter : UIBehaviour
    {
        [SerializeField]
        private List<WeaponLabel> _weaponLabels;
        private WeaponLabel _currentActive = null;
        private Player _player;
        private List<Weapon> _playerWeapons;

        private void Start()
        {
            _player = ServiceLocator<Player>.Get();
            _player.OnWeaponChanged += OnPlayerWeaponChanged;

            ObservePlayerWeapons();
        }

        private void ObservePlayerWeapons()
        {
            _playerWeapons = _player.Weapons;

            foreach (var weapon in _playerWeapons)
            {
                var weaponLable = _weaponLabels.FirstOrDefault(wl => wl.Type == weapon.Type);
                weaponLable.ObserveWeapon(weapon);
            }
        }

        private void OnPlayerWeaponChanged(Weapon weapon)
        {
            if (_currentActive != null)
            {
                _currentActive.HighlightAsUnactive();
            }

            _currentActive = _weaponLabels.FirstOrDefault(wl => wl.Type == weapon.Type);
            _currentActive.HighlightAsActive();
        }
    }
}