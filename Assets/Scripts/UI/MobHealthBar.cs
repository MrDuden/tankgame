﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class MobHealthBar : UIBehaviour
    {
        private HealthBar _healthBar = null;
        private Mob _mob = null;
        private int _mobHpCache = 0;
        private Camera _camera;
        private Pool _pool;

        private void Awake()
        {
            _camera = Camera.main;
            _mob = GetComponent<Mob>();
            _pool = ServiceLocator<Pool>.Get();
        }

        private void Update()
        {
            if (_healthBar == null) return;

            
            if (_mobHpCache != _mob.HP)
            {
                _mobHpCache = _mob.HP;
                _healthBar.SetBarValue((float)_mob.HP / _mob.MaxHP);
            }

            _healthBar.transform.LookAt(_camera.transform);
        }

        private void ReturnHealthBar()
        {
            _pool.Put<HealthBar>(_healthBar);
            this._healthBar = null;
        }

        private void OnEnable()
        {
            _healthBar = new  ProgressBarProvider().Get();
            _healthBar.transform.SetParent(transform);
            _healthBar.transform.position = _mob.HealthBarPoint.position;
        }

        private void OnDisable()
        {
            ReturnHealthBar();
        }
    }
}