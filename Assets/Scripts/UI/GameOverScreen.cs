﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TankGame
{
    public class GameOverScreen : UIBehaviour
    {
        [SerializeField]
        private Text _mobsCountLabel;

        public void Show()
        {
            gameObject.SetActive(true);
            _mobsCountLabel.text = ServiceLocator<PlayerLevelProgress>.Get().MobsKilled.ToString();
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }
    }
}