﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
namespace TankGame
{
    public class WeaponLabel : UIBehaviour
    {
        public WeaponType Type { get { return _type; } }
        [SerializeField]
        private HealthBar _reloadProgress;
        [SerializeField]
        private WeaponType _type;
        private Vector3 _startScale;
        private Vector3 _scalePunch = new Vector3(0.3f,0.3f,0.3f);
        private float _animSpeed = 0.1f;
        private Weapon _weapon = null;

        private void Awake()
        {
            _startScale = transform.localScale;
        }

        public void ObserveWeapon(Weapon weapon)
        {
            _weapon = weapon;
        }

        private void Update()
        {
            if (_weapon == null) return;

            if (!_weapon.IsReady)
                UpdateReloadBar((float)_weapon.ReloadProgress / _weapon.ReloadTime);
                
        }

        private void UpdateReloadBar(float factor)
        {
            if (factor >= 1)
            {
                _reloadProgress.gameObject.SetActive(false);
                return;
            }

            if (!_reloadProgress.gameObject.activeSelf)
                _reloadProgress.gameObject.SetActive(true);

            _reloadProgress.SetBarValue(factor);
        }

        public void HighlightAsActive()
        {
            transform.DOScale(_startScale + _scalePunch, _animSpeed);
        }

        public void HighlightAsUnactive()
        {
            transform.DOScale(_startScale - _scalePunch, _animSpeed);
        }
    }
}