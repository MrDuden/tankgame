﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TankGame
{
    public class PlayerHealthBar : UIBehaviour
    {
        [SerializeField]
        private HealthBar _healthBar;
        private Player _player;
        private int _healthCache = 0;

        public void Start()
        {
            _player = ServiceLocator<Player>.Get();    
        }

        private void Update()
        {
            if (_player.HP != _healthCache)
            {
                _healthCache = _player.HP;

                _healthBar.SetBarValue((float)_player.HP / _player.MaxHp);
            }
        }
    }
}