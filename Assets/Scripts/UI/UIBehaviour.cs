﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TankGame
{
    public abstract class UIBehaviour : MonoBehaviour
    {
        protected RectTransform _rect;

        private void Awake()
        {
            _rect = GetComponent<RectTransform>();
        }
    }
}