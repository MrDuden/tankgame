﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public abstract class Player : MonoBehaviour
    {
        public event Action<Weapon> OnWeaponChanged;
        public event Action OnKilled;

        public abstract int MaxHp { get; }
        public abstract int HP { get; }
        public abstract Weapon CurrentWeapon {get;}
        public abstract List<Weapon> Weapons {get;}

        public abstract void Move(float axis);
        public abstract void Rotate(float axis);
        public abstract void Fire();
        public abstract void PreviousWeapon();
        public abstract void NextWeapon();

        protected void RaiseKillEvent()
        {
            OnKilled?.Invoke();
        }

        protected void RaiseWeaponChangeEvent(Weapon weapon)
        {
            OnWeaponChanged?.Invoke(weapon);
        }
    }
}