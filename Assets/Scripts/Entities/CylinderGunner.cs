﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace TankGame
{
    public class CylinderGunner : Mob, IDamageable
    {
        public override MobType Type { get { return MobType.Gunner; } }

        [SerializeField]
        private Weapon _weapon;
        [SerializeField]
        private Agent _agent;
        [SerializeField]
        private float _attackDistance;
        [SerializeField]
        private Vector3 _scaleShakePunch = new Vector3(0.1f, 0.1f, 0.1f);
        [SerializeField]
        private float _scaleShankeDuration = 0.5f;
        [SerializeField]
        private float _attackAngle;
        private Player _player;
        private Pool _pool;
        
        public void Awake()
        {
            _stats = GetComponent<Stats>();
            _collider = GetComponent<Collider>();
        }

        private void Start()
        {
            _player = ServiceLocator<Player>.Get();
            _pool = ServiceLocator<Pool>.Get();
        }

        private void Update()
        {
            var heading = _player.transform.position - transform.position;
            var distanceToTarget = heading.magnitude;

            if (distanceToTarget < _attackDistance)
            {
                var directionToTarget = heading / distanceToTarget;

                if (RaycastOutsideBounds(directionToTarget, out var hit, distanceToTarget))
                {
                    if (hit.collider.gameObject == _player.gameObject)
                    {
                        if (Vector3.Angle(directionToTarget, transform.forward) < _attackAngle)
                        {
                            if (!_agent.IsStopped)
                                _agent.Stop();
 
                            Attack();
                        }
                        else
                        {
                            Stop();
                            _agent.RotateToTarget(_player.transform);
                        }
                    }
                    else
                    {
                        MoveTo(_player.transform.position);
                    }
                }
            }
            else
            {
                MoveTo(_player.transform.position);
            }
        }

        public override void Stop()
        {
            _agent.Stop();
        }

        public override void MoveTo(Vector3 position)
        {
            _agent.MoveTo(position);
        }

        public override void Attack()
        {
            if (_weapon.IsReady)
                _weapon.Fire();
        }

        public void OnSpawned()
        {
            Reset();
            RaiseSpawnedEvent();
        }

        public override void Kill()
        {
            RaiseKilledEvent();
            _pool.Put<CylinderGunner>(this);
        }

        private void Reset()
        {
            _stats.Reset();
        }

        void IDamageable.TakeDamage(int damage)
        {
            _stats.TakeDamage(damage);
            transform.DOPunchScale(_scaleShakePunch, _scaleShankeDuration);

            if (_stats.Health <= 0)
                Kill();
        }

        private void OnEnable()
        {
            OnSpawned();
        }
    }
}