﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;

namespace TankGame
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Stats))]
    public class Tank : Player, IDamageable
    {
        public override int MaxHp { get { return _stats.MaxHealth; } }
        public override int HP { get { return _stats.Health; } }
        public override List<Weapon> Weapons{get{ return _weapons; } }
        public override Weapon CurrentWeapon { get { return _currentWeapon; } }

        [SerializeField]
        private float _speed;
        [SerializeField]
        private float _rotationSpeed;
        [SerializeField]
        private List<Weapon> _weapons;
        [SerializeField]
        private Vector3 _scaleShakePunch = new Vector3(0.1f, 0.1f, 0.1f);
        [SerializeField]
        private float _scaleShankeDuration = 0.5f;

        private Weapon _currentWeapon;
        private int _currentWeaponIndex = 0;
        private Stats _stats;
        private Rigidbody _rb;

        private void Awake()
        {
            _weapons = GetComponentsInChildren<Weapon>().ToList();

            _stats = GetComponent<Stats>();
            _rb = GetComponent<Rigidbody>();
        }

        private void Start()
        {
            for (int i = 1; i < _weapons.Count; i++)
            {
                _weapons[i].Deactivate();
            }

            TakeWeapon(_weapons[_currentWeaponIndex]);
        }

        public override void NextWeapon()
        {
            if (_currentWeaponIndex >= _weapons.Count - 1)
                return;

            _weapons[_currentWeaponIndex].Deactivate();

            _currentWeaponIndex++;
            TakeWeapon(_weapons[_currentWeaponIndex]);
        }

        public override void PreviousWeapon()
        {
            if (_currentWeaponIndex == 0)
                return;

            _weapons[_currentWeaponIndex].Deactivate();

            _currentWeaponIndex--;
            TakeWeapon(_weapons[_currentWeaponIndex]);
        }

        public override void Fire()
        {
            _currentWeapon.Fire();
        }

        public override void Move(float axis)
        {
            _rb.velocity = transform.forward * axis *_speed;
        }

        public override void Rotate(float axis)
        {
            transform.Rotate(Vector3.up * _rotationSpeed * axis * Time.deltaTime);
        }

        void IDamageable.TakeDamage(int damage)
        {
            _stats.TakeDamage(damage);
            transform.DOPunchScale(_scaleShakePunch, _scaleShankeDuration);

            if (_stats.Health <= 0)
                OnPlayerKill();

        }

        private void OnPlayerKill()
        {
            RaiseKillEvent();
            gameObject.SetActive(false);
        }

        private void TakeWeapon(Weapon weapon)
        {
            _currentWeapon = weapon;
            _currentWeapon.Activate();

            RaiseWeaponChangeEvent(weapon);
        }
    }
}