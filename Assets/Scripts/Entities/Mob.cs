﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    [RequireComponent(typeof(Stats))]
    public abstract class Mob : MonoBehaviour
    {
        public static event Action<Mob> OnSpawned;
        public static event Action<Mob> OnKilled;

        public virtual int HP { get { return _stats.Health; } }
        public virtual int MaxHP { get { return _stats.MaxHealth; } }

        public Transform HealthBarPoint { get { return _healthBarPoint; } }
        public abstract MobType Type{get; }

        [SerializeField]
        protected Transform _healthBarPoint;
        protected Stats _stats;
        protected Collider _collider;

        public abstract void MoveTo(Vector3 position);
        public abstract void Stop();
        public abstract void Attack();
        public abstract void Kill();

        protected bool RaycastOutsideBounds(Vector3 direction, out RaycastHit hit, float distance)
        {
            var raycastStartPoint = transform.position + new Vector3(_collider.bounds.size.x * direction.x
                                                                        , 0, _collider.bounds.size.z * direction.z);

            if (Physics.Raycast(raycastStartPoint, direction, out hit, distance))
            {
                return true;
            }

            return false;
        }

        protected void RaiseKilledEvent() { OnKilled?.Invoke(this); }
        protected void RaiseSpawnedEvent() { OnSpawned?.Invoke(this); }
    }


    public enum MobType
    {
        CapsulePuncher,
        Gunner
    }
}