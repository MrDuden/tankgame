﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class StandalonePlayerController : BasePlayerController
    {
        public void Update()
        {
            if (!_isEnabled) return;

            _player.Rotate(Input.GetAxis("Horizontal"));

            if (Input.GetKeyDown(KeyCode.Q))
                _player.PreviousWeapon();
            else if (Input.GetKeyDown(KeyCode.E))
                _player.NextWeapon();
            else if (Input.GetKeyDown(KeyCode.X))
                _player.Fire();
        }

        public void FixedUpdate()
        {
            _player.Move(Input.GetAxis("Vertical"));
        }
    }
}