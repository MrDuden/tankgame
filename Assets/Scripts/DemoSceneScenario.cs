﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class DemoSceneScenario : MonoBehaviour
    {
        [SerializeField]
        private int _capsulePuncherTargetCount;
        [SerializeField]
        private int _gunnerTargetCount;
        private UIManager _uiManager;
        private EnemySpawnPoint[] _spawnPoints;
        private EnemySpawner _enemySpawner;
        private EnemyManager _enemyManager;
        private IMobData _capsulePuncherData;
        private IMobData _gunnerData;
        private List<BaseObjective> _objectives = new List<BaseObjective>();

        private void Start()
        {
            _spawnPoints = GameObject.FindObjectsOfType<EnemySpawnPoint>();
            _enemySpawner = ServiceLocator<EnemySpawner>.Get();
            _enemyManager = ServiceLocator<EnemyManager>.Get();
            _capsulePuncherData = _enemyManager.GetMobData(MobType.CapsulePuncher);
            _gunnerData = _enemyManager.GetMobData(MobType.Gunner);
            _uiManager = ServiceLocator<UIManager>.Get();
            _objectives.Add(new PlayerAliveRestriction());
        }

        private void Update()
        {
            SpawnMobs();
            TrackRestrictions();
        }

        private void TrackRestrictions()
        {
            foreach (var objective in _objectives)
            {
                if (objective.IsRestriction && objective.Achieved)
                    OnLevelFailed();
            }
        }

        private void OnLevelFailed()
        {
            _uiManager.GameOverScreen.Show();
        }

        private void SpawnMobs()
        {
            if (_capsulePuncherData.Count < _capsulePuncherTargetCount)
            {
                var mob = _enemySpawner.SpawnEnemy<CapsulePuncher>();
                mob.transform.position = _spawnPoints[Random.Range(0, _spawnPoints.Length - 1)].SpawnPoint;
            }

            if (_gunnerData.Count < _gunnerTargetCount)
            {
                var mob = _enemySpawner.SpawnEnemy<CylinderGunner>();
                mob.transform.position = _spawnPoints[Random.Range(0, _spawnPoints.Length - 1)].SpawnPoint;
            }
        }
    }
}