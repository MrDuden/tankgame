﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public interface IDamageable
    {
        void TakeDamage(int damage);
    }
}