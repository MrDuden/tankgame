﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class Rocket : Projectile
    {
        [SerializeField]
        private float _speed;
        [SerializeField]
        private Rigidbody _rb;
        private bool _isMoving = false;
        private Pool _pool;

        void Awake()
        {
            _pool = ServiceLocator<Pool>.Get();
        }

        public override void Launch()
        {
            _isMoving = true;
        }

        private void Update()
        {
            if (!_isMoving) return;

            _rb.velocity = transform.forward * _speed;
        }

        private void OnCollisionEnter(Collision collision)
        {
            _affector.Affect(collision.collider.gameObject);
            _pool.Put<Rocket>(this);
        }
    }
}