﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class Grenade : Projectile
    {
        public float Timer { get { return _timer; }set { _timer = value; } }

        [SerializeField]
        private float _timer;
        [SerializeField]
        private float _radius;
        [SerializeField]
        private LayerMask _collisionMask;
        private Coroutine _timerCoroutine;
        private Pool _pool;

        void Awake()
        {
            _pool = ServiceLocator<Pool>.Get();
        }

        public override void Launch()
        {
            gameObject.SetActive(true);
            _timerCoroutine = StartCoroutine(BlowDelay());
        }

        private void Blow()
        {
            var colliders = GetInRadiusColliders();

            foreach (var collider in colliders)
            {
                _affector.Affect(collider.gameObject);
            }

            _pool.Put<Grenade>(this);
        }

        private IEnumerator BlowDelay()
        {
            yield return new WaitForSeconds(_timer);
            Blow();
        }

        private Collider[] GetInRadiusColliders()
        {
            return Physics.OverlapSphere(transform.position, _radius);
        }
    }
}