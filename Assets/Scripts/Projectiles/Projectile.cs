﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public abstract class Projectile : MonoBehaviour
    {
        [SerializeField]
        protected Affector _affector;

        public abstract void Launch();
    }
}