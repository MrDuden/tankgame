﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public abstract class PrefabProvider
    {
        protected Pool _pool;
        protected Configuration _config;

        public PrefabProvider()
        {
            _pool = ServiceLocator<Pool>.Get();
            _config = ServiceLocator<Configuration>.Get();
        }
    }
}
