﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class GrenadeProvider : PrefabProvider
    {
        public Grenade Get()
        {
            var projectile = _pool.Get<Grenade>();

            if (projectile != null)
            {
                return projectile;
            }

            return _config.GrenadeConfig.Create();
        }
    }
}