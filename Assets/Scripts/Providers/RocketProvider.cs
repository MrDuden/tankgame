﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TankGame
{
    public class RocketProvider : PrefabProvider
    {
        public Rocket Get()
        {
            var projectile = _pool.Get<Rocket>();

            if (projectile != null)
            {
                return projectile;
            }

            return _config.RocketConfig.Create();
        }
    }
}