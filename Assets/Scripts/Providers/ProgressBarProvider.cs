﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class ProgressBarProvider : PrefabProvider
    {
        public HealthBar Get()
        {
            var healthBar = _pool.Get<HealthBar>();

            if (healthBar == null)
            { 
                healthBar = GameObject.Instantiate(_config.HealthBarPrefab);
            }

            return healthBar;
        }
    }
}