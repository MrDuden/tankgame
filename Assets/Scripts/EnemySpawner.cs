﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField]
        private List<Mob> _prefabs;
        private Pool _pool;

        private void Start()
        {
            _pool = ServiceLocator<Pool>.Get();
        }

        public T SpawnEnemy<T>() where T : Mob
        {
            T mob = _pool.Get<T>();

            if (mob == null)
            {
                foreach (var prefab in _prefabs)
                {
                    var type = prefab.GetType();

                    if (type == typeof(T))
                    {
                        mob = Instantiate((T)prefab);
                    }
                }
            }

            return mob;
        }
    }
}