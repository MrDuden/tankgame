﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class Stats : MonoBehaviour
    {
        public int MaxHealth { get { return _maxHealth; } }
        public int Health { get { return _health; } }

        [SerializeField]
        private int _maxHealth;
        [Range(0,1)]
        [SerializeField]
        private float _maxArmor;
        private int _health;
        private float _armor;

        private void Awake()
        {
            Reset();
        }

        public void Reset()
        {
            _health = _maxHealth;
            _armor = _maxArmor;
        }

        public void TakeDamage(int damage)
        {
            _health -= (int)(damage - (damage * _armor));
        }
    }
}
