﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public abstract class BasePlayerController : MonoBehaviour
    {
        protected Player _player;
        protected bool _isEnabled = true;

        private void Start()
        {
            _player = ServiceLocator<Player>.Get();    
        }

        public virtual void EnableControlls(bool status)
        {
            _isEnabled = status;
        }
    }
}
