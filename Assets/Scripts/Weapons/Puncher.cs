﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class Puncher : Weapon
    {
        public override int Ammo { get { return 0; } }
        public override AmmoType AmmoType { get { return AmmoType.None; } }

        public override WeaponType Type { get { return WeaponType.Puncher; } }

        [SerializeField]
        private LayerMask _targetLayer;
        [SerializeField]
        private float _radius;
        [SerializeField]
        private int _damage;

        public override void Fire()
        {
            if (!_isReady) { return; }

            Collider[] hitColliders = Physics.OverlapSphere(transform.position, _radius, _targetLayer);

            foreach (var collider in hitColliders)
            {
                var damageable = collider.GetComponent<IDamageable>();

                if (damageable != null)
                {
                    damageable.TakeDamage(_damage);
                }
            }

            _isReady = false;

            Reload();
        }
    }
}