﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public abstract class Weapon : MonoBehaviour
    {
        public abstract int Ammo { get; }
        public abstract AmmoType AmmoType { get; }
        public abstract WeaponType Type { get; }
        public float ReloadTime { get { return _reloadTime; } }
        public float ReloadProgress { get { return _reloadTimer; } }
        public bool IsInfinityAmmom { get { return _isInfinityAmmo; } }
        public bool IsReady { get { return _isReady; } }

        [SerializeField]
        protected Transform _shootPoint;
        [SerializeField]
        protected float _reloadTime;
        protected float _reloadTimer;
        protected bool _isReady = true;
        protected bool _isInfinityAmmo = false;
        protected Coroutine _reloadCoroutine = null;

        public abstract void Fire();

        public virtual void Activate()
        {
            gameObject.SetActive(true);

            if (!_isReady)
            {
                _reloadCoroutine = StartCoroutine(ReloadCoroutine());
            }
        }

        public virtual void Deactivate()
        {
            if(_reloadCoroutine != null)
                StopCoroutine(_reloadCoroutine);

            _reloadCoroutine = null;
            gameObject.SetActive(false);
        }
 
        protected void Reload()
        {
            _reloadCoroutine = StartCoroutine(ReloadCoroutine());
        }

        protected virtual IEnumerator ReloadCoroutine()
        {
            _isReady = false;

            while (_reloadTimer < _reloadTime)
            {
                _reloadTimer += Time.deltaTime;
                yield return null;
            }

            _reloadTimer = 0;
            _isReady = true;
        }
    }

    public enum AmmoType
    {
        None,
        Rocket,
        Grenade
    }

    public enum WeaponType
    {
        GrenadeLaucnher,
        RocketLauncher,
        Puncher
    }
}