﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class GrenadeLauncher : Weapon
    {
        public override int Ammo { get { return 0; } }

        public override AmmoType AmmoType { get { return _ammoType; } }

        public override WeaponType Type { get { return WeaponType.GrenadeLaucnher; } }

        private readonly AmmoType _ammoType = TankGame.AmmoType.Grenade;
        private GrenadeProvider _grenadeProvider;

        private void Start()
        {
            _grenadeProvider = new GrenadeProvider();
        }

        public override void Fire()
        {
            if (_isReady)
            {
                var grenade = _grenadeProvider.Get();
                grenade.transform.position = _shootPoint.position;
                grenade.Launch();

                _isReady = false;

                Reload();
            }
        }
    }
}

