﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class RocketLauncher : Weapon
    {
        public override int Ammo { get { return 0; } }
        public override AmmoType AmmoType { get { return _ammoType; } }

        public override WeaponType Type { get { return WeaponType.RocketLauncher; } }

        private readonly AmmoType _ammoType = TankGame.AmmoType.Grenade;
        private RocketProvider _rocketProvider;

        private void Start()
        {
            _rocketProvider = new RocketProvider();
        }

        public override void Fire()
        {
            if (_isReady)
            {
                var rocket = _rocketProvider.Get();
                rocket.transform.position = _shootPoint.position;
                rocket.transform.forward = _shootPoint.transform.forward;
                rocket.Launch();

                _isReady = false;

                Reload();
            }
        }
    }
}