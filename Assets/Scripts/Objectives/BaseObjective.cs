﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public abstract class BaseObjective
    {
        public abstract bool IsRestriction { get; }
        public abstract bool Achieved { get; }
    }
}