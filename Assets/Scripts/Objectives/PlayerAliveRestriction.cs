﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class PlayerAliveRestriction : BaseObjective, IDisposable
    {
        public override bool IsRestriction { get { return true; } }

        public override bool Achieved { get { return _achieved; } }

        private bool _achieved = false;
        private Player _player;

        public PlayerAliveRestriction()
        {
            _player = ServiceLocator<Player>.Get();
            _player.OnKilled += OnPlayerKilled;

        }

        private void OnPlayerKilled()
        {
            _achieved = true;
        }

        public void Dispose()
        {
            _player.OnKilled -= OnPlayerKilled;
        }
    }
}
