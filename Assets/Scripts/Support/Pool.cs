﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class Pool
    {
        private Dictionary<Type, Queue<MonoBehaviour>> _caches = new Dictionary<Type, Queue<MonoBehaviour>>();

        public T Get<T>() where T : MonoBehaviour
        {
            if (!_caches.ContainsKey(typeof(T)))
            {
                return null;
            }

            var items = _caches[typeof(T)];

            if (items.Count == 0) { return null; }

            var item = _caches[typeof(T)].Dequeue();
            item.gameObject.SetActive(true);

            return (T)item;
        }

        public void Put<T>(T item) where T : MonoBehaviour
        {
            item.gameObject.SetActive(false);

            if (_caches.ContainsKey(typeof(T)))
            {
                _caches[typeof(T)].Enqueue(item);
                return;
            }

            var itemsList = new Queue<MonoBehaviour>();
            itemsList.Enqueue(item); 

            _caches.Add(typeof(T), itemsList);
        }
    }
}