﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class ServiceLocator<T> where T : class
    {
        private static T _instance;

        public static void Set(T instance)
        {
            _instance = instance;
        }

        public static T Get()
        {
            return _instance;
        }
    }

}