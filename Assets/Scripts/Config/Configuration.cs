﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    [CreateAssetMenu(fileName = "Configuration", menuName = "Configuration", order = 1)]
    public class Configuration : ScriptableObject
    {
        public GrenadeConfig GrenadeConfig;
        public RocketConfig RocketConfig;
        public HealthBar HealthBarPrefab;
    }
}