﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    [CreateAssetMenu(fileName = "GrenadeConfig", menuName = "Projectiles/Grenade Config", order = 1)]
    public class GrenadeConfig : ScriptableObject
    {
        public Grenade Prefab;

        public Grenade Create()
        {
            var grenade = Instantiate(Prefab);
            return grenade;
        }
    }
}