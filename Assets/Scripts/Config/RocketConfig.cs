﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    [CreateAssetMenu(fileName = "RoketConfig", menuName = "Projectiles/Rocket Config", order = 1)]
    public class RocketConfig : ScriptableObject
    {
        public Rocket Prefab;

        public Rocket Create()
        {
            var rocket = Instantiate(Prefab);
            return rocket;
        }
    }
}
