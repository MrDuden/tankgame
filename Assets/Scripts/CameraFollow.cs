﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class CameraFollow : MonoBehaviour
    {
        [SerializeField]
        private GameObject _target;
        [SerializeField]
        private float _distance;
        [SerializeField]
        private float _height;

        private void LateUpdate()
        {
            if (_target == null) return;

            transform.position = _target.transform.position - Vector3.forward * _distance + Vector3.up * _height;
            transform.LookAt(_target.transform);
        }
    }
}