﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class SimpleAffector : Affector
    {
        public override void Affect(GameObject target)
        {
            var damageable = target.GetComponent<IDamageable>();

            if (damageable != null)
                damageable.TakeDamage(_damage);
        }
    }
}