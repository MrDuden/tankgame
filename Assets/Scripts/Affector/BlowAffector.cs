﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class BlowAffector : Affector
    {
        public override void Affect(GameObject target)
        {
            var damageable = target.GetComponent<IDamageable>();

            int damage = (int)(_damage / Mathf.Max(1,Vector3.Distance(target.transform.position, transform.position)));

            if (damageable != null)
                damageable.TakeDamage(damage);
        }
    }
}