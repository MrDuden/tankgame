﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public abstract class Affector : MonoBehaviour
    {
        public virtual int Damage { get { return _damage; } set { _damage = value; } }

        [SerializeField]
        protected int _damage;

        public abstract void Affect(GameObject target);
    }
}
