﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class MainSceneInstaller : MonoBehaviour
    {
        [SerializeField]
        private Configuration _configuration;
        [SerializeField]
        private Tank _tank;
        [SerializeField]
        private EnemyManager _enemyManager;
        [SerializeField]
        private EnemySpawner _enemySpawner;
        [SerializeField]
        private UIManager _uiManager;
        private void Awake()
        {
            InstallerServices();
        }

        private void InstallerServices()
        {
            ServiceLocator<Configuration>.Set(_configuration);
            ServiceLocator<Pool>.Set(new Pool());
            ServiceLocator<EnemyManager>.Set(_enemyManager);
            ServiceLocator<EnemySpawner>.Set(_enemySpawner);
            ServiceLocator<Player>.Set(_tank);
            ServiceLocator<PlayerLevelProgress>.Set(new PlayerLevelProgress());
            ServiceLocator<UIManager>.Set(_uiManager);
        }
    }
} 