﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankGame
{
    public class PlayerLevelProgress : IDisposable
    {
        public int MobsKilled { get { return _mobsKilled; } }

        private int _mobsKilled;

        public PlayerLevelProgress()
        {
            Mob.OnKilled += OnMobKilled;
        }

        private void OnMobKilled(Mob mob)
        {
            _mobsKilled++;
        }

        public void Dispose()
        {
            Mob.OnKilled -= OnMobKilled;
        }
    }
}